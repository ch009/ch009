#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void Reverse(char *,char *);
int PalindromeCheck(char *,char *);

int main()  {
	char cOrg[100];
	char cRev[100];
	int iCheck;
	printf("Please input a string\n");
	gets(cOrg);
	Reverse(cOrg,cRev);
	iCheck = PalindromeCheck(cOrg,cRev);
	if (iCheck == 1)
		printf("The String Is Palindrome\n");
	else
		printf("The String is not a palindrome\n");
	return 0;
}

void Reverse (char * original, char * reverse)  {
	int i = strlen(original)-1;
	int k = strlen(original);
	int j;
	for(j=0; original[j] != '\0';j++,i--)  {
		reverse[i] = original[j];
	}
	reverse[k] = '\0';
	printf("The Original String is : %s\n",original);
	printf("The Reverse String is : %s\n",reverse);
}

int PalindromeCheck(char *original, char *reverse)  {
	int i;
	int j;
	for (i = 0; original[i] != '\0';i++)  {
		if (original[i] == reverse[i])
			j = 1;
		else 
			j = 0;
	}
	return j;
}