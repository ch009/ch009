#include <stdio.h>

int main()  {
   int n;
   printf ("Please input the size of the array \n");
   scanf("%d",&n);
   int arr[n];
   printf("Please enter %d elements of the array \n",n);
   for(int i=0;i<n;i++)  {
      scanf("%d",&arr[i]);
   }
   int sum = 0;
   for(int j=0;j<n;j++)  {
      sum = sum + arr[j];
   }
   float avg = (float)sum/n;
   printf("Average of %d numbers = \t%f",n,avg);
   return 0;
}       