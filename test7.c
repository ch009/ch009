#include<stdio.h>  
#include<string.h>

void input();
void print();
void compare();

   struct student_info  {
         char name[50];
         char DOB[11];
         char course[10];
         int roll;
         int marks;
         int fees;
   };
     
    struct student_info stud1;
    struct student_info stud2;
 
 int main()  {
     input();
     print();
     compare();
 }

 void input()  {
    printf("Student 1 details:-\n");
    printf("Please enter the name\n");
    gets(stud1.name);
    printf("Please enter the DOB\n");
    gets(stud1.DOB);
    printf("Please enter the course\n");
    gets(stud1.course);
    printf("Please enter roll no.\n");
    scanf("%d",&stud1.roll);
    printf("Please enter the marks\n");
    scanf("%d",&stud1.marks);
    printf("Please enter the fees\n");
    scanf("%d",&stud1.fees);
    
    printf("\n\nStudent 2 details:-\n");
    printf("Please enter the name\n");
    scanf("%s",stud2.name);
    printf("Please enter the DOB\n");
    scanf("%s",stud2.DOB);
    printf("Please enter the course\n");
    scanf("%s",stud2.course);
    printf("Please enter roll no.\n");
    scanf("%d",&stud2.roll);
    printf("Please enter the marks\n");
    scanf("%d",&stud2.marks);
    printf("Please enter the fees\n");
    scanf("%d",&stud2.fees);
 }
 
 void print()  {
    printf("\n\nSTUDENT 1 DETAILS\n");
    printf("------------------\n");
    printf("Name     :%s\n",stud1.name);
    printf("DOB      :%s\n",stud1.DOB);
    printf("Roll No. :%d\n",stud1.roll);
    printf("Course   :%s\n",stud1.course);
    printf("Marks    :%d\n",stud1.marks);
    printf("Fees     :%d\n",stud1.fees);
    printf("\n\nSTUDENT 2 DETAILS\n");
    printf("------------------\n");
    printf("Name     :%s\n",stud2.name);
    printf("DOB      :%s\n",stud2.DOB);
    printf("Roll No. :%d\n",stud2.roll);
    printf("Course   :%s\n",stud2.course);
    printf("Marks    :%d\n",stud2.marks);
    printf("Fees     :%d\n",stud2.fees);
 }
 void compare()  {
     if(stud1.marks>stud2.marks)  {
         printf("\n\n%s got higher marks\n",stud1.name);
     }
     else if(stud1.marks<stud2.marks)  {
         printf("\n\n%s got higher marks\n",stud2.name);
     }
     else  {
         printf("\n\nBoth got same marks\n");
     }
 }