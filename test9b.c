#include<stdio.h>

int main()  {
	int p,q,sum,diff,prod,mod;
	int quo;
	int *a,*b;
	printf("Enter number 1 : ");
	scanf("%d",&p);
	printf("\nEnter number 2 : ");
	scanf("%d",&q);
    a = &p;
    b = &q;
	sum = *a + *b;
	diff = *a - *b;
	prod = *a * *b;
	mod = *a % *b;
	quo = (*a / *b);
	printf("\nSum of %d and %d = %d\n",*a,*b,sum);
	printf("Difference of %d and %d = %d\n",*a,*b,diff);
	printf("Product of %d and %d = %d\n",*a,*b,prod);
	printf("Quotient of %d and %d = %d\n",*a,*b,quo);
	printf("Modulus of %d and %d = %d\n",*a,*b,mod);
	return 0;
}
 