#include<stdio.h>

//Program to find an element by using binary search

int main()  {
    int n;
    int iLeft,iRight,iMid,iEle;
    int iArr[100];
    printf("Please enter the size of the array\n");
    scanf("%d",&n);
    printf("Please enter %d elements of the array\n",n);
    for (int i=0;i<n;i++)  {
      scanf("%d",&iArr[i]);
    }
    printf("Please enter the element to be searched\n");
    scanf("%d",&iEle);
    iLeft=0,iRight=n-1;
    while(iLeft<=iRight)  {
        iMid=(iLeft+iRight)/2;
        if(iArr[iMid]<iEle)  {
          iLeft=iMid+1;
        }  
        else if(iArr[iMid]==iEle)  {
            printf("Element %d found at %d position\n",iEle,iMid);
            break;
        }
        else if(iArr[iMid]>iEle)  {
          iRight=iMid-1;
        }
    }
    if(iLeft>iRight)  {
      printf("The Element is not found here!!\n");
    }
    return 0;
}