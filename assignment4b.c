#include<stdio.h>

int main () {
   int n,a=0,b=0,c=-1;//Since initial value of n = 0, counter for 0 (c) is set to -1
   while (n!=-1) {
      printf("Please input a number \n");
      scanf("%d",&n);
      if (n>=1)
         a++;
      else if (n<-1)
         b++;
      else
         c++;
   }
   printf("Number of positive integers = %d \n",a);
   printf("Number of negative integers = %d \n",b);
   printf("Number of zeros = %d \n",c);
   return 0;
}
   
   