#include<stdio.h>

struct name {
        char first[10];
        char middle[10];
        char last[10];
    };
    struct date {
        int date;
        char mnt[10];
        int yr;
    };
    struct employee {
        int id;
        int salary;
        struct name no1;
        struct date join;
    };
    struct employee emp1;
    
void Input();
void Print();

int main()  {
    Input();
    Print();
}

void Input()  {
    printf("Please Enter The Details\n");
    printf("First Name : ");
    scanf("%s",emp1.no1.first);
    printf("\nMiddle Name : ");
    scanf("%s",emp1.no1.middle);
    printf("\nLast Name : ");
    scanf("%s",emp1.no1.last);
    printf("\nDate Of Birth");
    printf("\nDate : ");
    scanf("%d",&emp1.join.date);
    printf("\nMonth : ");
    scanf("%s",emp1.join.mnt);
    printf("\nYear : ");
    scanf("%d",&emp1.join.yr);
    printf("\nEmployee Details");
    printf("\nEmployee Id : ");
    scanf("%d",&emp1.id);
    printf("\nSalary : ");
    scanf("%d",&emp1.salary);
}

void Print()  {
    printf("\n\n");
    printf("EMPLOYEE DETAILS\n");
    printf("\n----------------\n");
    printf("Name          : %s %s %s\n",emp1.no1.first,emp1.no1.middle,emp1.no1.last);
    printf("Date Of Birth : %d - %s - %d\n",emp1.join.date,emp1.join.mnt,emp1.join.yr);
    printf("Employee ID   : %d\n",emp1.id);
    printf("Salary        : Rs. %d \n",emp1.salary);
    printf("\n----------------\n");
}