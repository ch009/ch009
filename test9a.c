#include<stdio.h>

void Swap(int *, int *);

int main()  {
	int num1,num2;
	printf("Enter number 1 : ");
	scanf("%d",&num1);
	printf("\nEnter number 2 : ");
	scanf("%d",&num2);
	printf("Before Swapping : num1 = %d  num2 = %d\n",num1,num2);
	Swap(&num1,&num2);
	printf("After Swapping : num1 = %d  num2 = %d\n",num1,num2);
	return 0;
}

void Swap(int *a, int *b)  {
	int c;
	c = *a;
	*a = *b;
	*b = c;
}
