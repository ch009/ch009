#include<stdio.h>

/*A program to take array size from the user. The array is also
initialized by the user. The largest and the smallest numbers
are found and swapped.*/

int main()  {
    int n;
    printf("Please enter the size of the array \n");
    scanf("%d",&n);
    int arr[n];
    printf("Please enter %d elements of the array \n",n);
    for(int i=0;i<n;i++)  {
        scanf("%d",&arr[i]);
    }
    printf("Original Array is :-\n");
    for(int j=0;j<n;j++)  {
        printf("%d\t\n",arr[j]);
    }
    int a=arr[0],b=arr[0];
    int c=0,d=0;//Declared position markers
    for(int k=1;k<n;k++)  {
        if(arr[k]>a)  {
            a=arr[k];
            c=k;
        }
        if(b>arr[k])  {
            b=arr[k];
            d=k;
        }
    }
    printf("Largest element is %d found at %dth position \n",a,c);
    printf("Smallest element is %d found at %dth position \n",b,d);
    int e=0;
    e=arr[c];
    arr[c]=arr[d];//swapping Takes Place
    arr[d]=e;
    printf("Swapped Array is :-\n");
    for(int l=0;l<n;l++)  {
        printf("%d\t",arr[l]);
    }
    return 0;
}