#include<stdio.h>

//Program to insert an element in an array 
int main()  {
    int n=6;
    int pos =3,num=7;
    int arr[6]={2,3,5,11,13};
    printf("The original array :-\n");
    for(int i=0;i<n;i++)  {
        printf("%i\t",arr[i]);
    }
    for(int i=n-1;i>pos;i--)  {
        arr[i]=arr[i-1];
    }
    arr[pos]=num;
    printf("\nThe Array after insertion :-\n");
    for(int i=0;i<n;i++)  {
        printf("%i\t",arr[i]);
    }
    return 0;
}