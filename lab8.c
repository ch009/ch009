#include<stdio.h>

//Program to delete an element in an array

int main()  {
    int ind,n;
    printf("Please input the size of the array\n");
    scanf("%i",&n);
    int arr[n];
    printf("Please input %i elements of the array\n",n);
    for(int i=0;i<n;i++)  {
        scanf("%i",&arr[i]);
    }
    printf("The original array :-\n");
    for(int i=0;i<n;i++)  {
        printf("%i\t",arr[i]);
    }
    check:
    printf("\nPlease input the index of the number to be deleted\nPS:-index = position of the element-1\n");
    //index=position of the element-1
    scanf("%i",&ind);
    if (ind<n)  {
        for(int i=ind;i<n;i++)  {
        arr[i]=arr[i+1];
        }
        n=n-1; 
    }
    else  {
        printf("Incorrect index entered.....\nPlease enter a correct index\n");
        goto check; //goto loop is used incase the user enters a wrong index
    }
    printf("The Array after deletion :-\n");
    for(int i=0;i<n;i++)  {
        printf("%i\t",arr[i]);
    }
    return 0;
}