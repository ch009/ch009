#include<stdio.h>

int main()  {
    int m,n;
    printf("Please enter the no. of rows\n");
    scanf("%d",&m);
    printf("No. of rows entered =\t%d\n",m);
    printf("Please enter the no. of columns\n");
    scanf("%d",&n);
    printf("No. of columns entered =\t%d\n",n);
    int a[m][n],b[m][n],c[m][n],d[m][n];
    for(int i=0;i<m;i++)  {
        for(int j=0;j<n;j++)  {
            printf("Please enter [%d][%d] element of array 1\n",i,j);
            scanf("%d",&a[i][j]);
        }
    }
     for(int i=0;i<m;i++)  {
        for(int j=0;j<n;j++)  {
            printf("Please enter [%d][%d] element of array 2\n",i,j);
            scanf("%d",&b[i][j]);
        }
    }
    printf("Addition:-\n");
    for(int i=0;i<m;i++)  {
        for(int j=0;j<n;j++)  {
            c[i][j]=a[i][j]+b[i][j];
        }
    }
    for(int i=0;i<m;i++)  {
        for(int j=0;j<n;j++)  {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }   
    printf("\n%c%c%c%c+\n\n",32,32,32,32);//Printing a space using the ascii value 32
    for(int i=0;i<m;i++)  {
        for(int j=0;j<n;j++)  {
            printf("%d\t",b[i][j]);
        }
        printf("\n");
    }  
    printf("\n%c%c%c%c=\n\n",32,32,32,32);// A tab space is too big and unnecessary
    for(int i=0;i<m;i++)  {
        for(int j=0;j<n;j++)  {
            printf("%d\t",c[i][j]);
        }
        printf("\n");
    }    
    return 0;
}    