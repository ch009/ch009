#include <stdio.h>

//Program to find the desired element by binary search

int main()  {
    int n;
    printf ("Please enter the size of the array\n");
    scanf("%i",&n);
    int arr[n];
    printf("Please enter %d elements of the array\n",n);
    for(int i=0;i<n;i++)  {
        scanf("%d",&arr[i]);
    }
    int x,ind=-1;
    Check:
    printf("Please enter the element to be searched\n");
    scanf("%d",&x);
    for(int i=0;i<n;i++)  {
        if(x==arr[i])  {
            ind =i;
            break;
        }
    }
    if(ind>=0)  {
        printf("%d is found in %dth index and %dth position\n",x,ind,ind+1);
    }
    else  {
        printf("Element not found.....\n");
        goto Check;
    }
    return 0;
}