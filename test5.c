#include<stdio.h>

int main()  {
    int m,n;
    printf("Please enter the no. of rows\n");
    scanf("%d",&m);
    printf("No. of rows entered =\t%d\n",m);
    printf("Please enter the no. of columns\n");
    scanf("%d",&n);
    printf("No. of columns entered =\t%d\n",n);
    int a[m][n],trans[n][m];
    for(int i=0;i<m;i++)  {
        for(int j=0;j<n;j++)  {
            printf("Please enter [%d][%d] element of matrix\n",i,j);
            scanf("%d",&a[i][j]);
        }
    }
    for(int i=0;i<n;i++)  {
        for(int j=0;j<m;j++)  {
            trans[i][j]=a[j][i];
        }
    }
    printf("Original matrix:-\n");
    for(int i=0;i<m;i++)  {
        for(int j=0;j<n;j++)  {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    printf("Transpose matrix:-\n");
    for(int i=0;i<n;i++)  {
        for(int j=0;j<m;j++)  {
            printf("%d\t",trans[i][j]);
        }
        printf("\n");
    }
    return 0;

}